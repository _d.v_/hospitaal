import axios from 'axios'

const RESOURCE_PATH = 'http://localhost:3000/'

export default class HospitalService {
  getPatients() {
    return axios.get(RESOURCE_PATH + 'patients').then(res => res.data)
  }
  getPatient(id) {
    return axios.get(RESOURCE_PATH + 'patients/' + id).then(res => res.data)
  }
  getDepartment(id) {
    return axios.get(RESOURCE_PATH + 'departments/' + id).then(res => res.data)
  }
  getDepartments() {
    return axios.get(RESOURCE_PATH + 'departments').then(res => res.data)
  }
  getDoctor(id) {
    return axios.get(RESOURCE_PATH + 'doctors/' + id).then(res => res.data)
  }
  updatePatient(patient) {
    return axios.put(RESOURCE_PATH + 'patients/' + patient.id, patient)
  }
  getPicture(url) {
    return axios.get(RESOURCE_PATH + url).then(res => res.data)
  }
}
