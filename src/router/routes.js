
const routes = [
  {
    path: '/',
    redirect: '/home',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: 'home', name: 'index', component: () => import('pages/Index.vue') },
      { path: 'floorplan', name: 'hospital', component: () => import('pages/FloorPlan.vue') },
      { path: 'hospital/departments/:deptId', name: 'rooms', component: () => import('pages/Rooms.vue'), props: true },
      { path: 'hospital/patients/:patientId', name: 'roomDetail', component: () => import('pages/RoomDetail.vue'), props: true },
      { path: 'about', name: 'about', component: () => import('pages/About.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
